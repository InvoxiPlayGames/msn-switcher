#pragma once

#include <winsdkver.h>
#define _WIN32_WINNT _WIN32_WINNT_WINXP
#include <SDKDDKVer.h>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <WS2tcpip.h>
#include <WinInet.h>

#include <ShellAPI.h>

const char* ReplaceServer(const char*);

void AttachHooks();
void DetachHooks();
bool LoadConfig();

hostent* (WINAPI *real_gethostbyname)(const char*) = gethostbyname;
int (WINAPI *real_getaddrinfo)(PCSTR, PCSTR, const ADDRINFOA*, PADDRINFOA*) = getaddrinfo;
HINTERNET(WINAPI *real_InternetConnectA)(HINTERNET, LPCSTR, INTERNET_PORT, LPCSTR, LPCSTR, DWORD, DWORD, DWORD_PTR) = InternetConnectA;
HINTERNET(WINAPI *real_InternetConnectW)(HINTERNET, LPCWSTR, INTERNET_PORT, LPCWSTR, LPCWSTR, DWORD, DWORD, DWORD_PTR) = InternetConnectW;
BOOL(WINAPI *real_HttpQueryInfoA)(HINTERNET, DWORD, LPVOID, LPDWORD, LPDWORD) = HttpQueryInfoA;
BOOL(WINAPI *real_InternetSetCookieA)(LPCSTR, LPCSTR, LPCSTR) = InternetSetCookieA;
BOOL(WINAPI *real_ShellExecuteExA)(SHELLEXECUTEINFOA*) = ShellExecuteExA;
LONG(WINAPI *real_RegQueryValueExA)(HKEY, LPCSTR, LPDWORD, LPDWORD, LPBYTE, LPDWORD) = RegQueryValueExA;
HANDLE(WINAPI* real_CreateEventA)(LPSECURITY_ATTRIBUTES, BOOL, BOOL, LPCSTR) = CreateEventA;

hostent* WINAPI hook_gethostbyname(const char*);
int WINAPI hook_getaddrinfo(PCSTR, PCSTR, const ADDRINFOA*, PADDRINFOA*);
HINTERNET WINAPI hook_InternetConnectA(HINTERNET, LPCSTR, INTERNET_PORT, LPCSTR, LPCSTR, DWORD, DWORD, DWORD_PTR);
HINTERNET WINAPI hook_InternetConnectW(HINTERNET, LPCWSTR, INTERNET_PORT, LPCWSTR, LPCWSTR, DWORD, DWORD, DWORD_PTR);
BOOL WINAPI hook_HttpQueryInfoA(HINTERNET, DWORD, LPVOID, LPDWORD, LPDWORD);
BOOL WINAPI hook_InternetSetCookieA(LPCSTR, LPCSTR, LPCSTR);
BOOL WINAPI hook_ShellExecuteExA(SHELLEXECUTEINFOA*);
LONG WINAPI hook_RegQueryValueExA(HKEY, LPCSTR, LPDWORD, LPDWORD, LPBYTE, LPDWORD);
HANDLE WINAPI hook_CreateEventA(LPSECURITY_ATTRIBUTES, BOOL, BOOL, LPCSTR);