#include <winsdkver.h>
#define _WIN32_WINNT _WIN32_WINNT_WINXP
#include <SDKDDKVer.h>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <stdio.h>
#include <WS2tcpip.h>
#include <WinInet.h>
#include <ShellAPI.h>
#include <atlbase.h>
#include <detours.h>

#include "dllmain.h"
#include "ini.h"

#pragma comment(lib, "detours")
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "wininet.lib")

#define D(fmt, ...) { \
	if (FileDebug) { \
		fprintf(FileDebug, fmt, __VA_ARGS__); \
		fprintf(FileDebug, "\n"); fflush(FileDebug); \
	} \
};

static FILE* FileDebug = NULL;
static BOOL IsAttached = FALSE;

INT APIENTRY DllMain(HMODULE hDLL, DWORD Reason, LPVOID Reserved) {
	switch (Reason) {
	case DLL_PROCESS_ATTACH:
		DisableThreadLibraryCalls((HINSTANCE)hDLL);
		AttachHooks();
		break;
	case DLL_PROCESS_DETACH:
		DetachHooks();
		break;
	}
	return TRUE;
}

struct Config {
	bool debug;
	bool debugHooks;
	bool replacePassportURLs;
	bool enablePolygamy;
	const char* hookType;
	const char* overrideServer;
	const char* overrideConfigServer;
	int overrideServerLen;
	int overrideConfigServerLen;

	Config():
		debug(false), debugHooks(false), replacePassportURLs(true),
		hookType(nullptr), overrideServer(nullptr), overrideConfigServer(nullptr), overrideServerLen(0), overrideConfigServerLen(0)
	{
		this->overrideServer = "m1.escargot.log1p.xyz";
		this->overrideConfigServer = "escargot.log1p.xyz";
		this->overrideServerLen = strlen(this->overrideServer);
		this->overrideConfigServerLen = strlen(this->overrideConfigServer);
	}
};

static Config CONFIG;

void AttachHooks() {
	bool loaded = LoadConfig();
	
	if (CONFIG.debug) {
		HMODULE hModule = GetModuleHandleW(NULL);
		CHAR path[MAX_PATH];
		GetModuleFileNameA(hModule, path, MAX_PATH);
		strcat_s(path, "-escargot.log");
		fopen_s(&FileDebug, path, "a");
	}
	
	if (loaded) {
		if (CONFIG.hookType == NULL || !(strcmp(CONFIG.hookType, "msn") == 0 || strcmp(CONFIG.hookType, "yahoo") == 0)) {
			D("escargot.ini: hook type not specified or invalid");
			MessageBoxW(NULL, L"INI ERROR: Configuration does not specify hook type or specified hook type is invalid. Can't load Switcher.", L"Switcher", MB_ICONERROR | MB_OK);
			return;
		}
		D("escargot.ini: loaded");
	}
	else {
		D("escargot.ini: could not be loaded");
		MessageBoxW(NULL, L"INI ERROR: Could not load configuration file. Program will execute normally.", L"Switcher", MB_ICONERROR | MB_OK);
		return;
	}

	DetourRestoreAfterWith();
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourAttach(&(PVOID&)real_getaddrinfo, hook_getaddrinfo);
	if (strcmp(CONFIG.hookType, "msn") == 0) {
		if (CONFIG.replacePassportURLs) {
			DetourAttach(&(PVOID&)real_HttpQueryInfoA, hook_HttpQueryInfoA);
		}
		if (CONFIG.enablePolygamy) {
			DetourAttach(&(PVOID&)real_CreateEventA, hook_CreateEventA);
		}
	}
	DetourAttach(&(PVOID&)real_gethostbyname, hook_gethostbyname);
	DetourAttach(&(PVOID&)real_InternetConnectA, hook_InternetConnectA);
	DetourAttach(&(PVOID&)real_InternetConnectW, hook_InternetConnectW);
	if (strcmp(CONFIG.hookType, "yahoo") == 0) {
		DetourAttach(&(PVOID&)real_InternetSetCookieA, hook_InternetSetCookieA);
		DetourAttach(&(PVOID&)real_ShellExecuteExA, hook_ShellExecuteExA);
	}
	DetourAttach(&(PVOID&)real_RegQueryValueExA, hook_RegQueryValueExA);
	LONG error = DetourTransactionCommit();

	if (error == NO_ERROR) {
		IsAttached = TRUE;
		D("hooks: attached");
	}
}

void DetachHooks() {
	D("hooks: detaching");

	if (FileDebug) {
		fclose(FileDebug);
	}

	if (!IsAttached) return;

	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourDetach(&(PVOID&)real_getaddrinfo, hook_getaddrinfo);
	if (strcmp(CONFIG.hookType, "msn") == 0) {
		if (CONFIG.replacePassportURLs) {
			DetourDetach(&(PVOID&)real_HttpQueryInfoA, hook_HttpQueryInfoA);
		}
		if (CONFIG.enablePolygamy) {
			DetourAttach(&(PVOID&)real_CreateEventA, hook_CreateEventA);
		}
	}
	DetourDetach(&(PVOID&)real_gethostbyname, hook_gethostbyname);
	DetourDetach(&(PVOID&)real_InternetConnectA, hook_InternetConnectA);
	DetourDetach(&(PVOID&)real_InternetConnectW, hook_InternetConnectW);
	if (strcmp(CONFIG.hookType, "yahoo") == 0) {
		DetourDetach(&(PVOID&)real_InternetSetCookieA, hook_InternetSetCookieA);
		DetourDetach(&(PVOID&)real_ShellExecuteExA, hook_ShellExecuteExA);
	}
	DetourDetach(&(PVOID&)real_RegQueryValueExA, hook_RegQueryValueExA);
	LONG error = DetourTransactionCommit();

	if (error == NO_ERROR) {
		IsAttached = FALSE;
	}
}

static int configHandler(void* user, const char* section, const char* name, const char* value) {
	Config* config = (Config*)user;
	#define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
	if (MATCH("debug", "enable")) {
		config->debug = atoi(value) > 0;
	}
	else if (MATCH("debug", "hooks")) {
		config->debugHooks = atoi(value) > 0;
	}
	else if (MATCH("options", "passporturls")) {
		config->replacePassportURLs = atoi(value) > 0;
	}
	else if (MATCH("options", "polygamy")) {
		config->enablePolygamy = atoi(value) > 0;
	}
	else if (MATCH("options", "server")) {
		config->overrideServer = _strdup(value);
		config->overrideServerLen = strlen(value);
	}
	else if (MATCH("options", "configserver")) {
		config->overrideConfigServer = _strdup(value);
		config->overrideConfigServerLen = strlen(value);
	}
	else if (MATCH("options", "type")) {
		config->hookType = _strdup(value);
	}
	#undef MATCH
	return 1;
}

bool LoadConfig() {
	HMODULE hModule = GetModuleHandleW(NULL);
	CHAR path[MAX_PATH];
	GetModuleFileNameA(hModule, path, MAX_PATH);
	strcat_s(path, "-escargot.ini");
	return ini_parse(path, configHandler, &CONFIG) >= 0;
}

BOOL WINAPI hook_HttpQueryInfoA(HINTERNET a0, DWORD a1, LPVOID a2, LPDWORD a3, LPDWORD a4) {
	BOOL r = real_HttpQueryInfoA(a0, a1, a2, a3, a4);
	if (!r) return r;

	DWORD l = *a3;
	LPVOID data = a2;
	if (l < 50) return r;
	char* p = strstr((char*)data, "\nPassporturls:");
	if (p == NULL) return r;

	// Convert Passporturls to PassportURLs
	p += 1;
	strncpy_s(p, 12, "PassportURLs", _TRUNCATE);
	p[11] = 's';

	return r;
}

HANDLE WINAPI hook_CreateEventA(LPSECURITY_ATTRIBUTES a0, BOOL a1, BOOL a2, LPCSTR a3) {
	HANDLE r = real_CreateEventA(a0, a1, a2, a3);
	// Set to error code 0 if the event is Messenger related (checked on startup for multiple instances, usually 0xB7 if it's already open)
	SetLastError(0);
	return r;
}

HINTERNET WINAPI hook_InternetConnectA(HINTERNET a0, LPCSTR a1, INTERNET_PORT a2, LPCSTR a3, LPCSTR a4, DWORD a5, DWORD a6, DWORD_PTR a7) {
	LPCSTR a1orig = a1;
	a1 = ReplaceServer(a1orig);
	if (CONFIG.debugHooks) {
		D("InternetConnectA %s -> %s", a1orig, a1);
	}
	return real_InternetConnectA(a0, a1, a2, a3, a4, a5, a6, a7);
}

HINTERNET WINAPI hook_InternetConnectW(HINTERNET a0, LPCWSTR a1, INTERNET_PORT a2, LPCWSTR a3, LPCWSTR a4, DWORD a5, DWORD a6, DWORD_PTR a7) {
	USES_CONVERSION;
	LPCSTR a1orig = W2A(a1);
	LPCSTR a1a = ReplaceServer(a1orig);
	a1 = A2W(a1a);
	if (CONFIG.debugHooks) {
		D("InternetConnectW %s -> %ls", a1orig, a1);
	}
	return real_InternetConnectW(a0, a1, a2, a3, a4, a5, a6, a7);
}

hostent* WINAPI hook_gethostbyname(const char* a0) {
	const char* a0orig = a0;
	a0 = ReplaceServer(a0orig);
	if (CONFIG.debugHooks) {
		D("gethostbyname %s -> %s", a0orig, a0);
	}
	return real_gethostbyname(a0);
}

int WINAPI hook_getaddrinfo(PCSTR nn, PCSTR sn, const ADDRINFO* a, PADDRINFOA* b) {
	if (nn != NULL) {
		PCSTR nnorig = nn;
		nn = ReplaceServer(nnorig);
		if (CONFIG.debugHooks) {
			D("getaddrinfo %s -> %s", nnorig, nn);
		}
	}
	return real_getaddrinfo(nn, sn, a, b);
}

BOOL WINAPI hook_InternetSetCookieA(LPCSTR a0, LPCSTR a1, LPCSTR a2) {
	LPCSTR a0orig = a0;
	// Yahoo! won't use cookies sent by the server unless the domain in the cookie matches the one in the first argument (so the first argument must equal or encompass the replacement domain)
	if (strcmp(a0orig, "http://msg.edit.yahoo.com/config/ncclogin") == 0) {
		const char* s;
		int len;
		len = CONFIG.overrideServerLen;
		s = CONFIG.overrideServer;
		a0 = new char[7 + len + 17];
		snprintf((char*)a0, 7 + len + 17, "http://%s/config/ncclogin", s);
	}
	if (CONFIG.debugHooks) {
		D("InternetSetCookieA %s -> %s", a0orig, a0);
	}
	return real_InternetSetCookieA(a0, a1, a2);
}

BOOL WINAPI hook_ShellExecuteExA(SHELLEXECUTEINFOA* a0) {
	SHELLEXECUTEINFOA* a0orig = a0;
	LPCSTR forig = a0orig->lpFile;

	// "rd.yahoo.com" is opened by ShellExecuteExA for searches, so make sure we're only redirecting calls for that domain

	if (strncmp(forig, "http://rd.yahoo.com", 19) == 0) {
		int len;
		const char* s;
		len = CONFIG.overrideServerLen;
		s = CONFIG.overrideServer;

		int count = 7 + len;
		if (strlen(forig) > 19) {
			count += (strlen(forig) - 19);
		}
		count += 1;
		char* base = new char[7 + len + 1];
		LPCSTR f = new char[count];
		snprintf(base, 7 + len + 1, "http://%s", s);
		snprintf((char*)f, count, "%s%s", base, forig + 19);
		a0->lpFile = f;
	}
	if (CONFIG.debugHooks) {
		D("ShellExecuteExA %s -> %s", forig, a0->lpFile);
	}
	return real_ShellExecuteExA(a0);
}

LONG WINAPI hook_RegQueryValueExA(HKEY a0, LPCSTR a1, LPDWORD a2, LPDWORD a3, LPBYTE a4, LPDWORD a5) {
	LONG ret = real_RegQueryValueExA(a0, a1, a2, a3, a4, a5);
	if (ret != ERROR_SUCCESS) return ret;
	if (!(a1 && a5)) return ret;

	if (strcmp(CONFIG.hookType, "msn") == 0) {
		if (strcmp(a1, "Server") != 0) return ret;
		// MSN first does one call with a5 to get the length, then another call to get the value in a4
		if (a4 == NULL) {
			//TODO: Get accurate length of "Server" registry value
			*a5 = CONFIG.overrideServerLen + 20;
		}
		else {
			D("RegQueryValueExA %s", a1);
			int len;
			const char* s;
			len = CONFIG.overrideServerLen;
			s = CONFIG.overrideServer;
			strcpy_s((char*)a4, *a5, s);
			*a5 = len;
		}
	}
	else if (strcmp(CONFIG.hookType, "yahoo") == 0) {
		if (!(strcmp(a1, "Host Name") == 0 || strcmp(a1, "Socket Server") == 0 || strcmp(a1, "Server Name") == 0)) return ret;
		if (a4 != NULL) {
			D("RegQueryValueExA %s", a1);
			int len;
			const char* s;
			len = CONFIG.overrideServerLen;
			s = CONFIG.overrideServer;
			*a5 = len + 1;
			LPBYTE a4 = new BYTE[*a5];
			strcpy_s((char*)a4, *a5, s);
		}
	}
	return ret;
}

const char* ReplaceServer(const char* s) {
	bool shouldServerBeReplaced = FALSE;

	if (strcmp(CONFIG.hookType, "msn") == 0) {
		if (strcmp(s, "messenger.hotmail.com") == 0 || strcmp(s, "muser.messenger.hotmail.com") == 0 || strcmp(s, "gateway.messenger.hotmail.com") == 0 || strcmp(s, "muser.gateway.messenger.hotmail.com") == 0 || strcmp(s, "nexus.passport.com") == 0 || strcmp(s, "login.live.com") == 0 || strcmp(s, "msnia.login.live.com") == 0 || strcmp(s, "loginnet.passport.com") == 0 || strcmp(s, "g.msn.com") == 0 || strcmp(s, "svcs.microsoft.com") == 0 || strcmp(s, "config.messenger.msn.com") == 0 || strcmp(s, "muser.config.messenger.msn.com") == 0 || strcmp(s, "rsi.hotmail.com") == 0 || strcmp(s, "ows.messenger.msn.com") == 0 || strcmp(s, "byrdr.omega.contacts.msn.com") == 0 || strcmp(s, "omega.contacts.msn.com") == 0 || strcmp(s, "tkrdr.storage.msn.com") == 0 || strcmp(s, "sup.live.com") == 0) {
			shouldServerBeReplaced = TRUE;
		}
	}
	else if (strcmp(CONFIG.hookType, "yahoo") == 0) {
		if (strcmp(s, "scs.yahoo.com") == 0 || strcmp(s, "scs.msg.yahoo.com") == 0 || strcmp(s, "scsa.yahoo.com") == 0 || strcmp(s, "scsb.yahoo.com") == 0 || strcmp(s, "scsc.yahoo.com") == 0 || strcmp(s, "scsa.msg.yahoo.com") == 0 || strcmp(s, "scsb.msg.yahoo.com") == 0 || strcmp(s, "scsc.msg.yahoo.com") == 0 || strcmp(s, "chat.yahoo.com") == 0 || strcmp(s, "filetransfer.msg.yahoo.com") == 0 || strcmp(s, "vc.yahoo.com") == 0 || strcmp(s, "webcam.yahoo.com") == 0 || strcmp(s, "insider.msg.yahoo.com") == 0 || strcmp(s, "us.i1.yimg.com") == 0) {
			shouldServerBeReplaced = TRUE;
		}
	}
	if (shouldServerBeReplaced) {
		if (strcmp(CONFIG.hookType, "msn") == 0 && (strcmp(s, "config.messenger.msn.com") == 0 || strcmp(s, "muser.config.messenger.msn.com") == 0) && CONFIG.overrideConfigServerLen > 0) {
			return CONFIG.overrideConfigServer;
		}
		return CONFIG.overrideServer;
	}
	return s;
}

// DLLs don't get loaded unless at least one function is imported.
void WINAPI ImportMe() {}
